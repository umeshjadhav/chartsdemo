//
//  DailySensorJsonParser.swift
//  ChartsDemo
//
//  Created by Umesh on 08/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation

struct DailySensorJsonParserConst {
    static let dataKey = "data"
    static let dateKey = "dates"
    static let nameKey = "name"
    static let valuesKey = "values"
    static let avgKey = "avg"
    static let sumKey = "sum"
}

class DailySensorJsonParser {
    static func parse(filename: String, completion: @escaping (Graph) -> Void) {
        DispatchQueue.global(qos: .background).async {
            DataProvider.fetchData(filename: filename) { (response) in
                guard let responseDict = response as? [String: AnyObject] else { return }
                guard let dates = responseDict[DailySensorJsonParserConst.dateKey].map({ $0 }) as? [String] else { return }
                var formattedDates: [String] = []

                for (index, _) in dates.enumerated() {
                    formattedDates.append(dates[index].toDateString())
                }

                guard let sesonsorRawObjects = responseDict[DailySensorJsonParserConst.dataKey] else { return }
                let sensors = parseSensors(sesonsorRawObjects as! [AnyObject])

                DispatchQueue.main.async {
                    completion(Graph(sensors: sensors,
                                     dates: formattedDates))
                }
            }
        }
    }

   private static func parseSensors(_ sesonsorRawObjects: [AnyObject]) -> [SensorModel] {
        var sensorModels: [SensorModel] = []
        for i in 0..<sesonsorRawObjects.count {
            guard let sensorName = (sesonsorRawObjects[i] as AnyObject).value(forKey: DailySensorJsonParserConst.nameKey) as? String else {
                return []
            }

            if sensorName == Constant.Sensorname.windSpeed {
                if let sensor = sensorModel(sesonsorRawObjects[i] as AnyObject, DailySensorJsonParserConst.avgKey) {
                    sensorModels.append(sensor)
                }
            } else if sensorName == Constant.Sensorname.precipitation {
                if let sensor = sensorModel(sesonsorRawObjects[i] as AnyObject, DailySensorJsonParserConst.sumKey) {
                    sensorModels.append(sensor)
                }
            }
            if sensorModels.count == 2 {
                break
            }
        }
            return sensorModels
    }

    private static func sensorModel(_ dictionary: AnyObject,_ sensorValueKey: String) -> SensorModel? {
        guard let values = dictionary[DailySensorJsonParserConst.valuesKey] as? [String: AnyObject] else {
            return nil
        }
        guard let avg = values[sensorValueKey].map({ $0 }) else {
            return nil
        }
        return SensorModel(values: avg as! [Double])
    }
}
