//
//  DataProvider.swift
//  ChartsDemo
//
//  Created by Conchact on 07/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import UIKit

class DataProvider: NSObject {
    
    class func fetchData(filename: String, callBack: @escaping (Any) -> Void) {
            if let url = Bundle.main.url(forResource: filename, withExtension: Constant.JsonFile.fileType) {
                do {
                    let reponse = try Data(contentsOf: url)
                    let object = try JSONSerialization.jsonObject(with: reponse, options: .allowFragments)
                    guard let dictionary = object as? [String: AnyObject] else  { return }
                    callBack(dictionary)
                } catch {
                    print("Error!! Unable to parse  \(filename).json")
                    // TODO:- Add Proper Exception Handling.
                }
            }
    }
}
