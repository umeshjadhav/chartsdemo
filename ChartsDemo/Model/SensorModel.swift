//
//  File.swift
//  ChartsDemo
//
//  Created by Umesh on 06/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation

struct SensorModel {
    var values: [Double]
}

struct Graph {
    var sensors: [SensorModel]
    var dates: [String]
}
