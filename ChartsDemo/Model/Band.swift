//
//  Band.swift
//  ChartsDemo
//
//  Created by Umesh on 08/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation
import UIKit

class Band {
    static let defaultRange = 0
    let color: UIColor
    let rangeValue: Int
    var height: CGFloat = 0
    init(color: UIColor, rangeValue: Int = defaultRange) {
        self.color = color
        self.rangeValue = rangeValue
    }
}

struct Range {
    var min: Double = 0.0
    var max: Double?
}

struct GraphInfo {
    var range: Range = Range()
    var height: CGFloat {
        didSet {
            updateBandsHeight()
        }
    }
    var bands: [Band] = []

    private func updateBandsHeight() {
        let heightPerRange = getHeightPerRange()
        var remainingHeight = height
        for band in bands {
            if band.rangeValue <= Band.defaultRange {
                band.height = 0
            } else {
                band.height = heightPerRange * CGFloat(band.rangeValue)
            }
            remainingHeight -= band.height
        }
    }

    private func getHeightPerRange() -> CGFloat {
        guard let rangeMax = range.max else { return 0 }
        return height/CGFloat(rangeMax)
    }
}
