//
//  DumyTestData.swift
//  ChartsDemo
//
//  Created by Umesh on 08/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation
import UIKit

class DummyTestDataGenerator {
    static func bandsSample1() -> [Band] {
        var bands: [Band] = []
        bands.append(Band(color: Constant.Color.darkYellow, rangeValue: 3))
        bands.append(Band(color: Constant.Color.yellowShade, rangeValue: 1))
        bands.append(Band(color: Constant.Color.lightLemon, rangeValue: 2))
        bands.append(Band(color: Constant.Color.skyBlue, rangeValue: 1))
        bands.append(Band(color: Constant.Color.lightPurple, rangeValue: 1))
        bands.append(Band(color: Constant.Color.lightOrange, rangeValue: 1))
        bands.append(Band(color: Constant.Color.lightPink, rangeValue: 3))
        bands.append(Band(color: Constant.Color.greenShade, rangeValue: 3))
        return bands
    }

    static func bandsSample2() -> [Band] {
        var bands: [Band] = []
        bands.append(Band(color: Constant.Color.darkYellow, rangeValue: 3))
        bands.append(Band(color: Constant.Color.yellowShade, rangeValue: 3))
        bands.append(Band(color: Constant.Color.yellowShade, rangeValue: 3))
        bands.append(Band(color: Constant.Color.skyBlue, rangeValue: 3))
        bands.append(Band(color: Constant.Color.lightPink, rangeValue: 3))
        return bands
    }
}
