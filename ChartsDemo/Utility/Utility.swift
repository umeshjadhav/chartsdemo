//
//  Utility.swift
//  ChartsDemo
//
//  Created by Umesh on 07/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation
import Charts

extension String {
    func toDateString(withFormat format: String = Constant.DateFormatter.inputDateFormat) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        let date = dateformatter.date(from: self)
        dateformatter.dateFormat = Constant.DateFormatter.dateFormatForDaily
        let stringDate = dateformatter.string(from: date!)
        return stringDate
    }
}

extension CombinedChartView {

    static func getLineChartData(xValues: [String], yValuesLineChart: [Double]) -> LineChartData {
        var yVals: [ChartDataEntry] = [ChartDataEntry]()

        for i in 0..<xValues.count {
            yVals.append(ChartDataEntry(x: Double(i), y: yValuesLineChart[i]))
        }

        let lineChartSet = LineChartDataSet(values: yVals, label: Constant.Sensorname.windSpeed)
        lineChartSet.setColor(Constant.Color.cherryRed)
        lineChartSet.setCircleColor(Constant.Color.cherryRed)
        lineChartSet.lineWidth = 2.0
        lineChartSet.circleRadius = 2.5
        lineChartSet.highlightColor = UIColor.clear
        lineChartSet.drawCircleHoleEnabled = false
        lineChartSet.cubicIntensity = 0.1
        lineChartSet.drawValuesEnabled = false
        return LineChartData(dataSet: lineChartSet)
    }

    static func getBarChartData(xValues: [String], yValuesBarChart: [Double]) -> BarChartData {
        var yVals: [BarChartDataEntry] = [BarChartDataEntry]()

        for i in 0..<xValues.count {
            yVals.append(BarChartDataEntry(x: Double(i), yValues: [yValuesBarChart[i]]))
        }

        let barChartSet: BarChartDataSet = BarChartDataSet(values: yVals, label: Constant.Sensorname.precipitation)
        barChartSet.colors = [Constant.Color.barColor]
        barChartSet.barShadowColor = UIColor.clear
        barChartSet.drawValuesEnabled = false
        return BarChartData(dataSet: barChartSet)
    }
}
