//
//  Constant.swift
//  ChartsDemo
//
//  Created by Umesh on 08/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    struct Color {
        static let cherryRed = UIColor(red: 214.0/255.0, green: 48.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        static let barColor = UIColor(red: 1.0/255.0, green: 150.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        static let lightGreenish = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        static let skyBlue = UIColor(red: 129.0/255.0, green: 236.0/255.0, blue: 236.0/255.0, alpha: 1.0)
        static let lightPurple = UIColor(red: 162.0/255.0, green: 155.0/255.0, blue: 254.0/255.0, alpha: 1.0)
        static let lightLemon = UIColor(red: 255.0/255.0, green: 234.0/255.0, blue: 167.0/255.0, alpha: 1.0)
        static let lightOrange = UIColor(red: 250.0/255.0, green: 177.0/255.0, blue: 160.0/255.0, alpha: 1.0)
        static let lightPink = UIColor(red: 253.0/255.0, green: 121.0/255.0, blue: 168.0/255.0, alpha: 1.0)
        static let yellowShade = UIColor(red: 255.0/255.0, green: 236.0/255.0, blue: 139.0/255.0, alpha: 1.0)
        static let darkYellow = UIColor(red: 249.0/255.0, green: 180.0/255.0, blue: 45.0/255.0, alpha: 1.0)
        static let greenShade = UIColor(red: 102.0/255.0, green: 204.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    }

    struct JsonFile {
        static let daily = "Daily"
        static let fileType = "json"
    }

    struct Sensorname {
        static let windSpeed = "Wind speed"
        static let precipitation = "Precipitation"
    }

    struct DateFormatter {
        static let inputDateFormat = "yyyy-MM-dd HH:mm:ss"
        static let dateFormatForDaily = "MMM d"
    }

    static let noDataText = "Data not found for the chart."
}
