//
//  CustomYaxisRenderer.swift
//  ChartsDemo
//
//  Created by Umesh on 08/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import Foundation
import Charts

class CustomYaxisRenderer: YAxisRenderer {

    var graphInfo: GraphInfo = GraphInfo(range: Range(), height: 0.0, bands: [])

    open override func renderGridLines(context: CGContext) {
        guard let yAxis = axis as? YAxis else { return }

        if !yAxis.isEnabled {
            return
        }

        graphInfo.height = viewPortHandler.chartHeight
        drawHorizontalBands(context: context, yPointPositions: transformedPositions())
    }

   private func drawHorizontalBands(context: CGContext, yPointPositions: [CGPoint]) {
        guard yPointPositions.count >= 1 else { return }
        let heightPerPoint = (yPointPositions[1].y - yPointPositions[0].y) / 3
        var startIndexY = yPointPositions[0].y
        for i in 0..<graphInfo.bands.count {
            let band = graphInfo.bands[i]
            context.setStrokeColor(band.color.cgColor)
            context.setLineWidth(band.height)
            context.beginPath()
            let bandYAxis =  startIndexY - band.height/2
            context.move(to: CGPoint(x: viewPortHandler.contentLeft, y: bandYAxis))
            context.addLine(to: CGPoint(x: viewPortHandler.contentRight, y: bandYAxis))
            context.strokePath()
            startIndexY += CGFloat(band.rangeValue) * heightPerPoint
        }
    }
}
