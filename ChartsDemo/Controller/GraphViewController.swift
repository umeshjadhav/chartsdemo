//
//  ViewController.swift
//  ChartsDemo
//
//  Created by Umesh on 07/12/18.
//  Copyright © 2018 UJ. All rights reserved.
//

import UIKit
import Charts


class GraphViewController: UIViewController {
    
    @IBOutlet var graphView: CombinedChartView!
    private var customYAxisRenderer: CustomYaxisRenderer?

    override func viewDidLoad() {
        super.viewDidLoad()
        customiseGraphView()
        fetchGraphInfo()
    }
    //MARK:- Private methods

    private func updateChart(xValues: [String],
                         yValuesLineChart: [Double],
                         yValuesBarChart: [Double]) {
        graphView.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            return xValues[Int(index)]
        })

        graphView.data = combineChatData(xValues: xValues,
                                         yValuesLineChart: yValuesLineChart,
                                         yValuesBarChart: yValuesBarChart)
        graphView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInQuad)
        configureCustomYaxisRenderer()
    }

    private func combineChatData(xValues: [String],
                                 yValuesLineChart: [Double],
                                 yValuesBarChart: [Double]) -> CombinedChartData {
        let data: CombinedChartData = CombinedChartData()
        data.barData = CombinedChartView.getBarChartData(xValues: xValues, yValuesBarChart: yValuesBarChart)
        data.lineData = CombinedChartView.getLineChartData(xValues: xValues, yValuesLineChart: yValuesLineChart)
        return data
    }

    private func configureCustomYaxisRenderer() {
        customYAxisRenderer = CustomYaxisRenderer(viewPortHandler:graphView.viewPortHandler,
                                                  yAxis: graphView.leftAxis,
                                                  transformer: graphView.getTransformer(forAxis: graphView!.leftAxis.axisDependency))
        customYAxisRenderer!.graphInfo.bands = DummyTestDataGenerator.bandsSample1() // TODO:- Hook me with the UI
        graphView.leftYAxisRenderer = customYAxisRenderer!
        customYAxisRenderer!.graphInfo.range.max = graphView.leftAxis.axisMaximum
    }

    private func fetchGraphInfo() {
        DailySensorJsonParser.parse(filename: Constant.JsonFile.daily, completion: { (graph) in
            if graph.sensors.count >= 1 {
                self.updateChart(xValues: graph.dates,
                              yValuesLineChart: graph.sensors[1].values,
                              yValuesBarChart: graph.sensors[0].values)
            }
        })
    }

    private func customiseGraphView() {
        graphView.noDataText = Constant.noDataText
        graphView.drawBarShadowEnabled = false
        graphView.leftAxis.drawZeroLineEnabled = false
        graphView.leftAxis.spaceBottom = 0.0
        graphView.leftAxis.labelTextColor = UIColor.darkGray
        graphView.leftAxis.drawGridLinesEnabled = false
        graphView.leftAxis.gridColor = UIColor.clear
        graphView.rightAxis.enabled = true
        graphView.rightAxis.labelTextColor = UIColor.darkGray
        graphView.rightAxis.drawGridLinesEnabled = false
        graphView.xAxis.granularityEnabled = true
        graphView.xAxis.granularity = 1.0
        graphView.xAxis.avoidFirstLastClippingEnabled = false
        graphView.xAxis.labelTextColor = UIColor.darkGray
        graphView.xAxis.labelPosition = .bottom
        graphView.xAxis.drawGridLinesEnabled = false
        graphView.legend.enabled = true
        graphView.legend.textColor = UIColor.black
        graphView.fitScreen()
        graphView.scaleYEnabled = false
        graphView.doubleTapToZoomEnabled = false
        graphView.drawGridBackgroundEnabled = false
    }
}


